CFLAGS := `llvm-config --cppflags`
LINKFLAGS := `llvm-config --ldflags --libs core jit native`

BOOST := -I../../boost_1_48_0

main: statement.o lexer.o expression.o function.o compiler.o vm.o main.o
	g++ $^ `llvm-config --ldflags --libs core jit native` -ldl -o main

expression.o: expression.cpp expression_def.hpp expression.hpp
	g++ $(BOOST) -c $< 

function.o: function.cpp function_def.hpp function.hpp
	g++ $(BOOST) -c $< 

lexer.o: lexer.cpp lexer_def.hpp lexer.hpp
	g++ $(BOOST) -c $< 

statement.o: statement.cpp statement_def.hpp statement.hpp
	g++ $(BOOST) -c $<



compiler.o:compiler.cpp compiler.hpp
	g++ $(CFLAGS) $(BOOST) -c $<

vm.o: vm.cpp vm.hpp
	g++ $(CFLAGS) $(BOOST) -c $<

main.o: main.cpp
	g++ $(CFLAGS) $(BOOST) -c $<






